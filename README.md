Start binder [here](https://binderhub.k8s.flux.utah.edu/v2/git/https%3A%2F%2Fgitlab.flux.utah.edu%2Falex_orange%2Fshout-binder/master).

Go run the graph.ipynb file under the shout directory for pretty graphs!

Tested with shout commit bdd4d7e6c6dcd41b5880f2431dfb428ee5f3c4cf as of
shout-binder commit 86986f0abe3de52d1a50c4726f7cb41b14373b10.
