import sys
import argparse
import re

import os

import numpy as np
import scipy.signal as sig
import matplotlib.pyplot as plt
import h5py

from sigutils import *
from common import *

DEF_DATADIR="/calibration-data/mcondata"
DEF_DFNAME="measurements.hdf5"
MEAS_ROOT="measure_paths"
STATIC_ROOT="static_data"
DEF_FILTBW = 1e4

RATTRS = "_RUN_ATTRS"
DATA = "_DATA"
TXNAME = "_TXNAME"
RXNAME = "_RXNAME"

SITE_NAMES = ['bes', 'browning', 'dentistry', 'honors', 'hospital', 'fm', 'meb', 'smt', 'ustar']
#SITE_NAMES += ['bookstore', 'cpg', 'ebc', 'guesthouse', 'humanities', 'law73', 'madsen', 'moran', 'sagepoint', 'web']

SITE_PATTERNS = {
    'bes': r'-bes-?',
    'browning': r'-browning-?',
    'dentistry': r'-dentistry-?',
    'honors': r'-honors-?',
    'hospital': r'-hospital-?',
    'fm': r'-fm-?',
    'meb': r'-meb-?',
    'smt': r'-smt-?',
    'ustar': r'-ustar-?'
}

# Wrapper class to identify regex pattern string.
class RegexPattern(str):
    pass

class SiteName(str):
    pass

# Simple class to represent timestamp ranges
class TimestampRange:
    def __init__(self, tmin, tmax):
        self.min = int(tmin)
        self.max = int(tmax)

def calc_powerdiffs_from_samples(attrs, ds, filtbw):
    rate = attrs['rate']
    fstep = attrs['freq_step']
    steps = int(np.floor(rate/fstep/2))
    nsamps = attrs['nsamps']
    foff = filtbw/2
    pwrs = []
    for i in range(1,steps):
        bsamps = np.array(ds[0][(i-1)*nsamps:i*nsamps])
        tsamps = np.array(ds[1][(i-1)*nsamps:i*nsamps])
        fbsamps = butter_filt(bsamps, i*fstep - foff,
                              i*fstep + foff, rate)
        ftsamps = butter_filt(tsamps, i*fstep - foff,
                              i*fstep + foff, rate)
        pwrs.append(get_avg_power(ftsamps) - get_avg_power(fbsamps))
    return np.array(pwrs)

def do_psd_plots(attrs, name, allsamps):
    rate = attrs['rate']
    fstep = attrs['freq_step']
    steps = int(np.floor(rate/fstep/2))
    nsamps = attrs['nsamps']
    for i in range(1,steps):
        tsamps = allsamps[(i-1)*nsamps:i*nsamps]
        psd = compute_psd(nsamps, tsamps)
        freqs = np.fft.fftshift(np.fft.fftfreq(nsamps, 1/rate))
        title = "%s-%f" % (name, i*fstep)
        plot_proc(title, freqs, psd)

def search_entries(filters, results, name, obj):
    pelts = name.split('/')
    if len(filters) == len(pelts):
        i = 0
        for filt in filters:
            match = False
            if type(filt) not in (list, tuple):
                filt = [filt]
            for fent in filt:
                match = False
                if type(fent) == RegexPattern:
                    if re.match(fent, pelts[i]): match = True
                elif type(fent) == SiteName:
                    if get_site(pelts[i]) == fent: match = True
                elif type(fent) == TimestampRange:
                    try:
                        tstamp = int(pelts[i])
                        if tstamp <= fent.max and tstamp >= fent.min: match = True
                    except ValueError:
                        pass
                else:
                    if fent == '*' or fent == pelts[i]: match = True
                if match:
                    break
            i += 1
            if not match:
                return None
            elif i == len(pelts):
                results.append(obj)
    return None

def calc_measdiffs(objs):
    diffs = []
    for obj in objs:
        ent = {}
        run = obj.parent.parent
        ent[RATTRS] = run.attrs
        ent[TXNAME] = obj.parent.name.split('/')[-1]
        ent[RXNAME] = obj.name.split('/')[-1]
        #if args.usesamps:
        if False:
            ent[DATA] = calc_powerdiffs_from_samples(run.attrs, obj['samples'],
                                                     DEF_FILTBW)
        else:
            ent[DATA] = obj['avgpower'][1] - obj['avgpower'][0]
        diffs.append(ent)
    return diffs

def get_site(name):
    rval = name
    srch = list(filter(lambda a: re.search(a[1],name), SITE_PATTERNS.items()))
    if srch and len(srch):
        rval = srch[0][0]
    return rval

def get_distance(s1, s2, distdata):
    rval = 1
    if s1 in distdata:
        d1 = distdata[s1]
        if s2 in d1.attrs:
            rval = d1.attrs[s2]
    return rval

def plot_measdiffs(distdata, diffs):
    sites = {}
    means = []
    dists = []
    for d in diffs:
        txname = get_site(d[TXNAME])
        rxname = get_site(d[RXNAME])
        if not txname in sites:
            sites[txname] = {}
        if not rxname in sites[txname]:
            sites[txname][rxname] = []
        sites[txname][rxname].append(d[DATA])
    for txname,rxset in sites.items():
        for rxname,data in rxset.items():
            x = get_distance(txname, rxname, distdata)
            y = np.mean(sites[txname][rxname])
            err = np.std(sites[txname][rxname])
            plt.errorbar(x, y, yerr=err, fmt='rx')
            plt.annotate("%s,%s" % (txname, rxname), (x,y))
            dists.append(x)
            means.append(y)
    m, b = np.polyfit(dists, means, 1)
    plt.plot(dists, m*np.array(dists) + b, label = "slope: %f" % m)
    plt.legend()
    plt.show()

def plot_diffbars(diffs):
    sites = {}
    cnt = {}
    w = 0.15
    colors = ['midnightblue','cornflowerblue','darkcyan','royalblue','deepskyblue','cadetblue','darkturquoise','steelblue','teal']
    for d in diffs:
        txname = get_site(d[TXNAME])
        rxname = get_site(d[RXNAME])
        if not txname in sites:
            sites[txname] = {}
        if not rxname in sites[txname]:
            sites[txname][rxname] = []
        sites[txname][rxname].append(d[DATA])
    i = 0.0
    lbls = []
    fig, ax = plt.subplots()
    for txname, rxset in sites.items():
        for rxname,data in rxset.items():
            lbls.append("%s\n%s" % (txname, rxname))
            avgs = np.mean(sites[txname][rxname],0)
            stds = np.std(sites[txname][rxname],0)
            l = len(sites[txname][rxname][0])
            xstep = w*(l + 3)
            for k in range(l):
                off = i + w*(k - l/2)
                v = avgs[k] if avgs[k] > 0 else 0
                ax.bar(off, v, w, color=colors[k % len(colors)],
                       yerr=[[0],[stds[k]]])
            i += xstep
    ax.set_xticks(np.arange(i-xstep/2, step=xstep))
    ax.set_xticklabels(lbls)
    plt.xticks(rotation=90)
    plt.ylabel("Power difference (dB)")
    plt.show()
    fig.tight_layout()
    
def main(data_file, site_filter):
    filters = []
    tsmin = -1
    tsmax = -1
    tstamp = None

    tstamp = '*'
    filters.append(tstamp)
    if site_filter != '*':
        txname = SiteName(site_filter)
    else:
        txname = '*'
    filters.append(txname)
    rxname = '*'
    filters.append(rxname)

    dsfile = h5py.File(os.path.join(DEF_DATADIR, data_file), "r")

    results = []
    filters.insert(0, MEAS_ROOT)
    dsfile.visititems(lambda name, obj:
                      search_entries(filters, results, name, obj))
    diffs = calc_measdiffs(results)
    for d in diffs:
        txname = get_site(d[TXNAME])
        rxname = get_site(d[RXNAME])
    plproc2 = plot_diffbars(diffs)
